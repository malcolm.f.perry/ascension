#include "Controller/AscensionController.h"
#include "Kismet/GameplayStatics.h"
#include "Game/AscensionGameMode.h"
#include "Player/AscensionPlayerState.h"

void AAscensionController::SpawnCharacter()
{
	// Get the game mode to determine which pawn we spawn
	if (HasAuthority())
	{
		UWorld* World = GetWorld();
		AAscensionGameMode* GameMode = Cast<AAscensionGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
		AAscensionPlayerState* State = GetPlayerState<AAscensionPlayerState>();

		// TODO: Turn static string into a FName on the game mode
		if (World && GameMode && State && GameMode->IsLevelPlayable())
		{
			AAscensionCharacter* SpawnedActor = Cast<AAscensionCharacter>(World->SpawnActor(
				GameMode->GetPlayerPawn(State->GetPlayerInfo().Gender).Get(), &GameMode->FindPlayerStart(this)->GetTransform()));
			if (SpawnedActor)
			{
				Possess(SpawnedActor);
			}
		}
	}
}

void AAscensionController::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	// Setup a binding to spawn the character after the player state is created
	AAscensionPlayerState* State = GetPlayerState<AAscensionPlayerState>();
	if (State)
	{
		State->PostBeginPlay.AddUniqueDynamic(this, &AAscensionController::SpawnCharacter);
	}
}

void AAscensionController::BeginPlay()
{
	Super::BeginPlay();
	SetInputMode(FInputModeGameAndUI());
}