#include "Player/AscensionPlayerState.h"
#include "Game/AscensionGameInstance.h"
#include "../Ascension.h"

const FPlayerInfo AAscensionPlayerState::GetPlayerInfo()
{
	return PlayerInfo;
}

void AAscensionPlayerState::BeginPlay()
{
	// Retrieve player information from the game instance
	UAscensionGameInstance* GameInstance = Cast<UAscensionGameInstance>(GetGameInstance());
	if (GameInstance)
	{
		PlayerInfo = GameInstance->PlayerInfo;
	}

	Super::BeginPlay();

	PostBeginPlay.Broadcast();
}