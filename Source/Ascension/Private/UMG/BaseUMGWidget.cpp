#include "UMG/BaseUMGWidget.h"

UAscensionGameInstance* UBaseUMGWidget::GetAscensionGameInstance()
{
	return Cast<UAscensionGameInstance>(GetGameInstance());
}