#include "Actor/GoalArea.h"
#include "Game/AscensionGameState.h"

// Sets default values
AGoalArea::AGoalArea()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AGoalArea::BeginPlay()
{
	Super::BeginPlay();

	// Register with the game state
	UWorld* World = GetWorld();
	AAscensionGameState* GameState = World ? Cast<AAscensionGameState>(World->GetGameState()) : nullptr;
	if (GameState)
	{
		GameState->RegisterGoalArea(this);
	}
	else
	{
		UE_LOG(LogAscension, Error, TEXT("Could not get the GameState in the GoalArea."));
	}
}

// Called every frame
void AGoalArea::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}