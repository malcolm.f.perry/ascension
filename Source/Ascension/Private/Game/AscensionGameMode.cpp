#include "Game/AscensionGameMode.h"
#include "Kismet/GameplayStatics.h"

AAscensionGameMode::AAscensionGameMode()
{
}

TSubclassOf<AAscensionCharacter> AAscensionGameMode::GetPlayerPawn(EGender Gender)
{
    if (Gender == EGender::Female)
    {
        return FemaleCharacter;
    }
    else
    {
        return MaleCharacter;
    }
}

bool AAscensionGameMode::IsLevelPlayable() const
{
    return PlayLevels.Contains(FName(*UGameplayStatics::GetCurrentLevelName(GetWorld())));
}