#include "Game/AscensionGameState.h"
#include "Game/AscensionGameMode.h"
#include "Character/AscensionCharacter.h"
#include "Kismet/GameplayStatics.h"
#include "Player/PlayerHUD.h"

void AAscensionGameState::RegisterGoalArea(AGoalArea* NewGoalArea)
{
	GoalArea = NewGoalArea;
}

void AAscensionGameState::OnGameStart_Implementation()
{
	const AAscensionGameMode* GameMode = Cast<AAscensionGameMode>(GetDefaultGameMode());
	if (GameMode)
	{
		SetupGameTimer(GameMode->GameDuration);
	}
}

void AAscensionGameState::OnGameEnd_Implementation()
{
	if (GoalArea)
	{
		TArray<AActor*> ContainedActors;
		GoalArea->GetOverlappingActors(ContainedActors, AAscensionCharacter::StaticClass());

		if (ContainedActors.Num() == 0)
		{
			// No winner

			// Display message to all players -> everyone loses
			DisplayMessageToAllPlayers(FText::FromString("You Lose."));

			// Quit to main menu
			QuitToMainMenuWithDelay(10.0f);
		}
		else if (ContainedActors.Num() == 1)
		{
			// 1 Winner

			AAscensionCharacter* Character = Cast<AAscensionCharacter>(ContainedActors[0]);
			APlayerController* Controller = Character ? Cast<APlayerController>(Character->GetController()) : nullptr;

			// Display message to winner -> you win
			DisplayMessageToPlayer(FText::FromString("You Win!"), Controller);

			// Display message to others -> you lose
			TArray<APlayerController*> ExcludedPlayers;
			ExcludedPlayers.Add(Controller);
			DisplayMessageToPlayers(FText::FromString("You Lose."), ExcludedPlayers);

			// Quit to main menu
			QuitToMainMenuWithDelay(10.0f);
		}
		else
		{
			// More than 1 person in area

			// Display "there can be only 1 winner" message to all players
			DisplayMessageToAllPlayers(FText::FromString("There can be only 1 winner..."));

			// Add another minute to the timer
			SetupGameTimer(60.0);
		}
	}
	else
	{
		UE_LOG(LogAscension, Error, TEXT("No GoalArea found in the AscensionGameState."));
	}
}

void AAscensionGameState::DisplayMessageToPlayer(const FText& Message, APlayerController* Controller)
{
	APlayerHUD* Hud = Controller ? Cast<APlayerHUD>(Controller->GetHUD()) : nullptr;
	if (Hud)
	{
		Hud->SendSystemMessage(Message);
	}
}

void AAscensionGameState::DisplayMessageToPlayers(const FText& Message, TArray<APlayerController*> PlayersToExclude)
{
	UWorld* World = GetWorld();
	if (World)
	{
		for (FConstPlayerControllerIterator Iterator = World->GetPlayerControllerIterator(); Iterator; ++Iterator)
		{
			APlayerController* Controller = Iterator->Get();
			if (!PlayersToExclude.Contains(Controller))
			{
				DisplayMessageToPlayer(Message, Controller);
			}
		}
	}
}

void AAscensionGameState::DisplayMessageToAllPlayers(const FText& Message)
{
	DisplayMessageToPlayers(Message, TArray<APlayerController*>());
}

void AAscensionGameState::SetupGameTimer(float Duration)
{
	UWorld* World = GetWorld();
	if (World)
	{
		// Set the game timer
		World->GetTimerManager().SetTimer(GameTimerHandle, this, &AAscensionGameState::OnGameEnd, Duration, false);
	}
}

void AAscensionGameState::QuitToMainMenuWithDelay(float Delay)
{
	UWorld* World = GetWorld();
	if (World)
	{
		World->GetTimerManager().SetTimer(GameTimerHandle, this, &AAscensionGameState::QuitToMainMenu, Delay, false);
	}
	else
	{
		UE_LOG(LogAscension, Error, TEXT("Error: No World found!"));
	}
}

void AAscensionGameState::QuitToMainMenu()
{
	// Open the main menu level
	UGameplayStatics::OpenLevel(GetWorld(), "Lvl_MainMenu");
}

void AAscensionGameState::BeginPlay()
{
	Super::BeginPlay();
}