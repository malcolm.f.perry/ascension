#include "Ascension.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, Ascension, "Ascension" );
 
// Setup the AscensionPrototype general logging category
DEFINE_LOG_CATEGORY(LogAscension);