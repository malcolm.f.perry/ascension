#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "../Ascension.h"
#include "GoalArea.generated.h"

UCLASS()
class ASCENSION_API AGoalArea : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AGoalArea();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
};