#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "AscensionPlayerInfo.generated.h"

UENUM(BlueprintType)
enum class EGender : uint8
{
	Female UMETA(DisplayName = "Female"),
	Male UMETA(DisplayName = "Male")
};

USTRUCT(Blueprintable)
struct FPlayerInfo
{
	GENERATED_BODY()

	UPROPERTY(BlueprintReadWrite)
	FName Name;

	UPROPERTY(BlueprintReadWrite)
	EGender Gender = EGender::Female;

	FPlayerInfo() {}
};

/**
 * Wrapper object for the PlayerInfo struct
 */
UCLASS(MinimalApi)
class UAscensionPlayerInfo : public UObject
{
	GENERATED_BODY()
};