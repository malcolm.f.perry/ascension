#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "../Ascension.h"
#include "Player/AscensionPlayerInfo.h"
#include "AscensionPlayerState.generated.h"

// Event broadcast at the end of the begin player function
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FPostBeginPlay);

/**
 * The replicated state for the player. Holds the public player info (name, stats, etc.)
 */
UCLASS()
class ASCENSION_API AAscensionPlayerState : public APlayerState
{
	GENERATED_BODY()
	
public:
	FPostBeginPlay PostBeginPlay;

	const FPlayerInfo GetPlayerInfo();

protected:
	UPROPERTY(BlueprintReadOnly)
	FPlayerInfo PlayerInfo;

	void BeginPlay() override;
};