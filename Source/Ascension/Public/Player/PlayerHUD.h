#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "PlayerHUD.generated.h"

/**
 * Base class for the player HUD
 */
UCLASS()
class ASCENSION_API APlayerHUD : public AHUD
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void SendSystemMessage(const FText& Message);

protected:
	void BeginPlay() override;
};