#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "../Ascension.h"
#include "AscensionController.generated.h"

UCLASS()
class ASCENSION_API AAscensionController : public APlayerController
{
	GENERATED_BODY()

public:
	// Called after the player state has initialized to spawn and posses the player character
	UFUNCTION()
	void SpawnCharacter();

	// After player state and other components have been initialized
	void PostInitializeComponents() override;

protected:
	void BeginPlay() override;
};