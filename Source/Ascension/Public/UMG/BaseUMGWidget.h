#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Game/AscensionGameInstance.h"
#include "BaseUMGWidget.generated.h"

/**
 * Base class for all of the UI elements. Holds shared functions and styling.
 */
UCLASS()
class ASCENSION_API UBaseUMGWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable)
	UAscensionGameInstance* GetAscensionGameInstance();
};