#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "Player/AscensionPlayerInfo.h"
#include "AscensionGameInstance.generated.h"

/**
 * Holds information that should transition between levels.
 */
UCLASS()
class ASCENSION_API UAscensionGameInstance : public UGameInstance
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadWrite)
	FPlayerInfo PlayerInfo;
};