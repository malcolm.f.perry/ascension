#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameState.h"
#include "../Ascension.h"
#include "Actor/GoalArea.h"
#include "AscensionGameState.generated.h"

/**
 * Game state information, keeps track of things like timers and what to do to transition to different phases. Created by the GameMode.
 */
UCLASS()
class ASCENSION_API AAscensionGameState : public AGameState
{
	GENERATED_BODY()
	
public:
	UPROPERTY(BlueprintReadOnly)
	FTimerHandle  GameTimerHandle;

	UFUNCTION()
	void RegisterGoalArea(AGoalArea* NewGoalArea);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void OnGameStart();
	virtual void OnGameStart_Implementation();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void OnGameEnd();
	virtual void OnGameEnd_Implementation();

protected:
	UPROPERTY(BlueprintReadOnly)
	AGoalArea* GoalArea;

	UFUNCTION(BlueprintCallable)
	void DisplayMessageToPlayer(const FText& Message, APlayerController* Controller);

	UFUNCTION(BlueprintCallable)
	void DisplayMessageToPlayers(const FText& Message, TArray<APlayerController*> PlayersToExclude);

	UFUNCTION(BlueprintCallable)
	void DisplayMessageToAllPlayers(const FText& Message);

	void SetupGameTimer(float Duration);

	void QuitToMainMenuWithDelay(float Delay);

	void QuitToMainMenu();

	void BeginPlay() override;
};