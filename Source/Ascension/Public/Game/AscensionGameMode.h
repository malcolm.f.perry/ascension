#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameMode.h"
#include "Player/AscensionPlayerInfo.h"
#include "Character/AscensionCharacter.h"
#include "AscensionGameMode.generated.h"

/**
 * Game configuration information (like time limit), spawned for each level.
 */
UCLASS(minimalapi)
class AAscensionGameMode : public AGameMode
{
	GENERATED_BODY()

public:
	AAscensionGameMode();

	// Game time duration in seconds
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ascension Properties")
	float GameDuration = 300;

	// List of levels that can be played in (where a character is spawned)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ascension Properties")
	TArray<FName> PlayLevels;

	// The female character mesh
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Ascension Properties")
	TSubclassOf<AAscensionCharacter> FemaleCharacter;

	// The male character mesh
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Ascension Properties")
	TSubclassOf<AAscensionCharacter> MaleCharacter;

	TSubclassOf<AAscensionCharacter> GetPlayerPawn(EGender Gender);

	bool IsLevelPlayable() const;
};