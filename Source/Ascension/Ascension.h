#pragma once

#include "CoreMinimal.h"

// Setup the Ascension general logging category
DECLARE_LOG_CATEGORY_EXTERN(LogAscension, Log, All);

#define GET_STRING_FROM_ENUM(etype, evalue) ( (FindObject<UEnum>(ANY_PACKAGE, TEXT(etype), true) != nullptr) ? FindObject<UEnum>(ANY_PACKAGE, TEXT(etype), true)->GetNameStringByIndex((int32)evalue) : FString("Invalid - are you sure enum uses UENUM() macro?") )